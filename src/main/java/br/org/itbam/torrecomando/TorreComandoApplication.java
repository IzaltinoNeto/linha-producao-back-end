package br.org.itbam.torrecomando;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorreComandoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TorreComandoApplication.class, args);
	}

}
