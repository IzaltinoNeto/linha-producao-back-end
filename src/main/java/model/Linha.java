package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="linha")
public class Linha {
    @Id
    private int id;

    @OneToMany(mappedBy = "linha", fetch = FetchType.EAGER)
    private List<Estacao> estacoes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Estacao> getEstacoes() {
        return estacoes;
    }

    public void setEstacoes(List<Estacao> estacoes) {
        this.estacoes = estacoes;
    }
}
