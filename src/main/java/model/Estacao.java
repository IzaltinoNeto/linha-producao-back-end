package model;

import javax.persistence.*;

@Entity
@Table(name = "estacao")
public class Estacao {
    @Id
    private int id;

    private String status;

    private String error;

    @ManyToOne
    @JoinColumn(name = "linha_id")
    private Linha linha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
