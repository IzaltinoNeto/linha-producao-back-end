package model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;


public class Area{
    @Id
    @Column( columnDefinition = "area_id int(11)", unique = true, nullable = false)
    private int id;
    @Column(name = "nome", unique = true, nullable = false)
    private String nome;

    @OneToMany()
    private List<Linha> linhas;

}
